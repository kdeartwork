/***************************************************************************
                          resources.h  -  description
                             -------------------
    begin                : Thu Sep 6 2001
    copyright            : (C) 2001 by Henning Burchardt
    email                : h_burchardt@gmx.net
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/

#ifndef RESOURCES_H
#define RESOURCES_H

#define DEFAULT_CLOSE_BUTTON_COLOR Qt::red
#define DEFAULT_MAXIMIZE_BUTTON_COLOR Qt::yellow
#define DEFAULT_ICONIFY_BUTTON_COLOR Qt::green
#define DEFAULT_HELP_BUTTON_COLOR Qt::white
#define DEFAULT_STICKY_BUTTON_COLOR Qt::white

#endif

